import sys
# Remove current dir from sys.path, otherwise setuptools will peek up our
# module instead of system's.
sys.path.pop(0)
from setuptools import setup
sys.path.append("..")
import sdist_upip

# Get the long description from the README file
from os import path
with open(path.join(path.dirname(__file__), 'README.rst'), 'r') as f:
    long_description = f.read()


setup(name='micropython-structured-config',
      version='2.2',
      description='Configuration file support for micropython where the config structure is solely defined in python.',
      long_description=long_description,
      url='https://gitlab.com/alelec/micropython-structured-config',
      author='Andrew Leech',
      author_email='andrew@alelec.net',
      maintainer='Andrew Leech',
      maintainer_email='andrew@alelec.net',
      license='MIT',
      cmdclass={'sdist': sdist_upip.sdist},
      packages=['structured_config'])
